<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

<!-- ABOUT THE PROJECT -->
## About The Project

This is a technical exercise for software engineering candidates. 
For the exercise, use this documentation to build an API backend service, for managing a simple mini wallet. 

### Built With

This section should list any major frameworks that you built your project using. Leave any add-ons/plugins for the acknowledgements section. Here are a few examples.
* [Laravel](https://laravel.com)
* [PostgreSQL](http://www.postgresql.org)

And supporting tools to easy testing like :
* [Postman](https://www.postman.com)

<!-- GETTING STARTED -->
## Getting Started
So, let start it.
1. After clone this repository, just run `composer update`.
2. Setup your `.env` file such as database connection depend on you existing device.
3. To make sure that all dependency is run well, than run `php artisan migrate` to doing migration and migrate all databse that we provide in this project.
4. Than you can run `php artisan db:seed` if you need seeder for user, or you can manualy register user with register endpoint.
4. Finally, you can run your project with command: `php artisan serve`.
5. Go to postman and set url like `http://localhost:8000/api/`, for information that port to run this project depend on configuratin on `.env`

## Afterword
Hopefully, it can be easily understood and useful. Thank you~

